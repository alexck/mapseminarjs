import { SuperCluster, IReduceFunctions } from "./clustering";
import { WorkerMessage, MessageType, DataRequestParameters } from "./workerMessage";
import { Config } from "./config";

init();

function init()
{
    let reduceFunctions : IReduceFunctions = {
        accInitial : (origProps) => {
            let dataAvailable = origProps.info.snowDepthAvailable;
            let snowDepthData : number = parseFloat(origProps.info.snowDepth);
            let snowDepthInCm : number = snowDepthData / 10;
            let numberOfValidItems = dataAvailable ? 1 : 0;

            return {      
                numberOfValidPoints : numberOfValidItems,  
                snowDepth : snowDepthInCm,
                minSnowDepth : snowDepthInCm,
                maxSnowDepth : snowDepthInCm
            };
        },
        accFunction : (geoProps, currentProps) => {
            let snowDepthData : number = geoProps.snowDepth + currentProps.snowDepth;
            let minSnowDepthData : number = Math.min(geoProps.minSnowDepth, currentProps.minSnowDepth);
            let maxSnowDepthData : number = Math.max(geoProps.maxSnowDepth, currentProps.maxSnowDepth);
            let numberOfValidItems = geoProps.numberOfValidPoints + currentProps.numberOfValidPoints;
            let snowDepthFloor : number = Math.floor(snowDepthData);

            return {      
                numberOfValidPoints : numberOfValidItems,  
                snowDepth : snowDepthData,
                minSnowDepth : minSnowDepthData,
                maxSnowDepth : maxSnowDepthData
            };
        },
        accFinalize : (numberOfNeighbors, geoProps) => {
            let numberOfValidItems : number = geoProps.numberOfValidPoints;
            let snowDepthData : number = 0;

            if (numberOfValidItems > 0)
            {
                numberOfValidItems = 1;
                snowDepthData = parseFloat((geoProps.snowDepth / geoProps.numberOfValidPoints).toFixed(2));
            }
            return {
                numberOfValidPoints : numberOfValidItems,
                snowDepth : snowDepthData,
                snowDepthFloor : Math.floor(snowDepthData),
                minSnowDepth : geoProps.minSnowDepth,
                maxSnowDepth : geoProps.maxSnowDepth
            };
        }
    };

    let supercluster = new SuperCluster(0, 16, Config.getClusterMapRadius(), 512, 64, true, reduceFunctions);

     addEventListener('message', (message) => {
        let convertedMessage : WorkerMessage = message.data as WorkerMessage;

        switch (convertedMessage.type)
        {
            case MessageType.LoadData :
                supercluster.load(convertedMessage.parameters);
                break;
            case MessageType.UpdateClusterRadius:
                supercluster.setRadius(convertedMessage.parameters);
                break;
            case MessageType.RequestData :
                try
                {
                    let requestParameters : DataRequestParameters = convertedMessage.parameters as DataRequestParameters;
                    let calculatedGeoJson = supercluster.getClustersGeoJson(requestParameters.boundingBox, requestParameters.zoom);
                    let response : WorkerMessage = {
                        type : MessageType.WorkerGeoDataResponse,
                        parameters : calculatedGeoJson
                    };
                    postMessage(JSON.stringify(response));
                    break;
                }
                catch(e)
                {
                    console.log("Tried to get cluster data before initialization");
                } 
        }
    });
}
