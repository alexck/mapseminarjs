import { DateJson } from "./parser";
import { Config } from "./config";

export class DateManager
{
    private _validDates : DateJson;
    private _currentIndex : number

    constructor(validDates : DateJson)
    {
        this._validDates = validDates;
        this._currentIndex = 0;
    }

    public getCurrentDate() : string
    {
        return this._validDates.dates[this._currentIndex];
    }

    public getCurrentDateIndex() : number
    {
        return this._currentIndex;
    }

    public getCurrentDatePrettyPrint() : string
    {
        let originalDate = this._validDates.dates[this._currentIndex];
        let parts : string[] = originalDate.split("-");
        let dayInMonth : string = parts[0];
        let monthInYear : string = parts[1];
        return dayInMonth + "." + monthInYear + "." + Config.getDatasetYear().toString();
    }

    public getDatesCount() : number
    {
        return this._validDates.dates.length;
    }

    public increaseDate() : void
    {
        if (this._currentIndex < this.getDatesCount() - 1)
        {
            this._currentIndex++;
        }
        else
        {
            this._currentIndex = 0;
        }
    }

    public setDateByIndex(index : number) : void
    {
        if (index > this.getDatesCount() - 1)
        {
            throw new RangeError("Index is out of bounds");
        }
        this._currentIndex = index;
    }
}