### Prototyp zur Darstellung von GHCN Daily Daten mithilfe von Mapbox ###

### Build ###
Es handelt sich um ein Typescript Projekt, welches mit Webpack compiled wird.

Zum compilen ist node.js notwendig, Anleitung zur Installation unter: https://nodejs.org/en/

Zum compilen in das Stammverzeichnis dieses Repos wechseln und je nach Betriebssystem ausführen:

Linux:
```
./node_modules/webpack/bin/webpack.js
```

Windows:
```
node ".\node_modules\webpack\bin\webpack.js"
```

### Benutzung ###
Zur Benutzung sind folgende Daten notwendig:

* Konvertierte GHCN Daten, erzeugbar mithilfe dieses [Tools](https://bitbucket.org/alexck/mapseminar/overview) ODER
* Im Ordner out befindet sich eine 7zip Datei. Diese im Ordner out extrahieren um die Daten des Jahres 2016 zu nutzen
* Ein Webserver mit aktiviertem Cross-Origin Resource Sharing (CORS)

### Einfacher Webserver mit aktiviertem CORS ###
Zur Installation ist npm notwendig.
Zur Installation folgenden Befehl ausführen
```
npm install http-server -g
```
Danach im Stammverzeichnis dieses Repositories Folgendes ausführen
```
http-server --cors
```

Falls dieser Befehl unter Windows nicht funktioniert
```
node http-server --cors
```

In der Konsole erscheint die URL, zu welcher mithilfe des Webbrowsers navigiert werden soll.

Der Prototyp wurde mithilfe von Firefox und Google Chrome getestet.