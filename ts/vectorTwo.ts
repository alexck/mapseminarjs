import * as Util from "./helper";

export class Vector2
{
    private _xPos : number;
    private _yPos : number

    constructor(xPos : number, yPos : number)
    {
        this._xPos = xPos;
        this._yPos = yPos;
    }

    get xPos()
    {
        return this._xPos;
    }

    get yPos()
    {
        return this._yPos;
    }

    get longitudeFromMercator()
    {
        return Util.mercatorToLongitude(this._xPos);
    }

    get latitudeFromMercator()
    {
        return Util.mercatorToLatitude(this._yPos);
    }

    public convertToArray() : number[]
    {
        return [this._xPos, this._yPos];
    }

    public static createMercatorVector(longitude : number, latitude : number) : Vector2
    {
        return new Vector2(Util.longitudeToMercator(longitude), Util.latitudeToMercator(latitude));
    }
}
