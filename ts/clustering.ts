import * as kdbush from "kdbush";
import * as GeoJson from "@types/geojson";
import { Cluster, IClusterProperties, createClusterByPoint, createCluster } from "./cluster";
import * as Util from "./helper";
import { JsonParser } from "./parser";
import { Vector2 } from "./vectorTwo";

/*
 * Dieser Code basiert auf Code der Supercluster Library
 * Authoren: Vladimir Agafonkin, Andrew Harvey, Github Nutzer: jingsam
 * Source Code online unter: https://github.com/mapbox/supercluster/blob/master/index.js
 * Zuletzt gesichtet: 02.07.2017 
 */
export class SuperCluster
{
    private _minZoom : number;
    private _maxZoom : number;
    private _radius : number;
    private _extend : number;
    private _nodeSize : number;
    private _clusterTree : any[];
    private _points : GeoJSON.Feature<GeoJSON.Point>[];
    private _shouldAccumulate : boolean;
    private _reduceFunctions : IReduceFunctions;

    constructor(minZoom : number = 0, maxZoom : number = 16, radius : number = 50, extend : number = 512,
                nodeSize : number = 64, shouldAccumulate : boolean = false, reduceFunctions? : IReduceFunctions)
    {
        this._minZoom = minZoom;
        this._maxZoom = maxZoom;
        this._radius = radius;
        this._extend = extend;
        this._nodeSize = nodeSize;
        this._clusterTree = new Array(maxZoom + 1);
        this._shouldAccumulate = shouldAccumulate;
        this._reduceFunctions = reduceFunctions;
    }

    public setRadius(radius : number)
    {
        this._radius = radius;
        this.load(this._points);
    }

    public load(points : GeoJson.Feature<GeoJson.Point>[]) : void
    {
        this._points = points;
        let clusters : Cluster[] = new Array();
        
        for (let i = 0; i < points.length; i++)
        {
            if (this._shouldAccumulate)
            {
                clusters.push(createClusterByPoint(points[i], i, this._reduceFunctions.accInitial(points[i].properties)));
            }
            else
            {
                clusters.push(createClusterByPoint(points[i], i));
            }
        }
        let getClusterXPos : (cluster : Cluster) => number = (cluster : Cluster) =>
        {
            return cluster.pos.xPos;
        };
        let getClusterYPos : (cluster : Cluster) => number = (cluster : Cluster) =>
        {
            return cluster.pos.yPos;
        };
        this._clusterTree[this._maxZoom + 1] = kdbush(clusters, getClusterXPos, getClusterYPos, this._nodeSize);

        for (let i = this._maxZoom; i >= this._minZoom; i--)
        {
            clusters = this.cluster(clusters, i);
            this._clusterTree[i] = kdbush(clusters, getClusterXPos, getClusterYPos, this._nodeSize);
        }
    }

    public getClustersGeoJson(boundingBox : Util.BoundingBox, zoom : number) : GeoJson.FeatureCollection<GeoJson.Point>
    {
        return JsonParser.getGeoJsonFromClusters(this.getClusters(boundingBox, zoom));
    }

    public getClusters(boundingBox : Util.BoundingBox, zoom : number) : Cluster[]
    {
        if (!this._points)
        {
            throw new TypeError("Clusters not loaded yet");
        }
        let tree : any = this._clusterTree[this.clampZoom(zoom)];
        let mercatorBoundingBox = Util.mapboxBoundsToMercator(boundingBox);
        let ids = tree.range(mercatorBoundingBox.minXPos, mercatorBoundingBox.minYPos, mercatorBoundingBox.maxXPos, mercatorBoundingBox.maxYPos);
        let clusters : Cluster[] = new Array();

        for (let currentId of ids)
        {
            let currentCluster : Cluster = tree.points[currentId];
            clusters.push(currentCluster);
        }
        return clusters;
    }

    private cluster(currentClusters : Cluster[], clusterZoomLevel : number) : Cluster[]
    {
        let clusters : Cluster[] = new Array();
        let r = this._radius / (this._extend * Math.pow(2, clusterZoomLevel));

        for (let i = 0 ; i < currentClusters.length; i++)
        {
            let point : Cluster = currentClusters[i];

            if (point.zoom <= clusterZoomLevel)
            {
                continue;
            }
            point.zoom = clusterZoomLevel;
            let tree : any = this._clusterTree[clusterZoomLevel + 1];
            let neighborIds : number[] = tree.within(point.pos.xPos, point.pos.yPos, r);
            let numPoints : number = point.numberOfPoints;
            let wX : number = point.pos.xPos * numPoints;
            let wY : number = point.pos.yPos * numPoints;
            let initialGeoProp : any;

            if (this._shouldAccumulate)
            {
                initialGeoProp = point.clusterProperties.reducedProperties;      
            }
            
            for (let currentNeighborId of neighborIds)
            {
                let currentNeighbor : Cluster = tree.points[currentNeighborId];

                if (currentNeighbor.zoom <= clusterZoomLevel)
                {
                    continue;
                }
                currentNeighbor.zoom = clusterZoomLevel;
                let neighborNumPoints = currentNeighbor.numberOfPoints;
                wX += currentNeighbor.pos.xPos * neighborNumPoints;
                wY += currentNeighbor.pos.yPos * neighborNumPoints;
                numPoints += neighborNumPoints;
                currentNeighbor.parentId = i;

                if (this._shouldAccumulate)
                {
                    initialGeoProp = this._reduceFunctions.accFunction(currentNeighbor.clusterProperties.reducedProperties, initialGeoProp);
                }
            }

            if (numPoints === 1)
            {
                clusters.push(point);
            }
            else
            {
                point.parentId = i;
                if (this._shouldAccumulate)
                {
                    initialGeoProp = this._reduceFunctions.accFinalize(neighborIds.length, initialGeoProp);
                    clusters.push(createCluster(new Vector2(wX / numPoints, wY / numPoints), i, numPoints, initialGeoProp));
                }
                else
                {
                    clusters.push(createCluster(new Vector2(wX / numPoints, wY / numPoints), i, numPoints));
                }
            }
        }
        return clusters;
    }

    private clampZoom(zoom : number) : number
    {
        return Math.round(Math.max(this._minZoom, Math.min(zoom, this._maxZoom + 1)));
    }
}

export interface IReduceFunctions
{
    accInitial : (originalGeoProps : any) => any,
    accFunction : (geoProps : any, currentProps : any) => any,
    accFinalize : (numberOfNeighbors : number, geoProps : any) => any
}
