import mapboxgl = require('mapbox-gl');
import { RectangleSelector } from "./rectangleSelector";
import { Config } from "./config";
import * as GeoJson from "@types/geojson";

export class MapManager
{
    private _map : mapboxgl.Map;
    private _layerIds : string[];
    private _sourceInit : boolean;
    private _currentMode : MapModes;

    constructor()
    {
        mapboxgl.accessToken = "pk.eyJ1IjoiYWxleGNla2F5IiwiYSI6ImNqMmNnZXh0eTAyam8zNnF5ODk2bGNwN24ifQ.P_ThU0oEHmHFDg19eK56ag";

        this._map = new mapboxgl.Map({
            container : "map",
            style: 'mapbox://styles/mapbox/bright-v9',
            center: [-103.59179687498357, 40.66995747013945],
            zoom: 3
        });
        this._layerIds = new Array();
        this._sourceInit = false;
        this._map.on("load", () => {
            this._map.dragRotate.disable();
            this.updateSource(this.getDataForInit());
            this.switchStyle(MapModes.Cluster);   
        });      
        this.showClusterPopup = this.showClusterPopup.bind(this);
        this.setCursorToPointer = this.setCursorToPointer.bind(this);
        this.setCursorToNormal = this.setCursorToNormal.bind(this);
    }

    public disableDragPan() : void
    {
        this._map.dragPan.disable();
    }

    public enableDragPan() : void
    {
        this._map.dragPan.enable();
    }

    public getMapCanvas() : HTMLElement
    {
        return this._map.getCanvasContainer();
    }

    public queryMapByBoundingBox(bbox : number[][]) : GeoJson.Feature<GeoJson.GeometryObject>[]
    {
        return this._map.queryRenderedFeatures(bbox, { layers: this._layerIds});
    }

    public updateSource(geoJson : any) : void
    {
        if (this._sourceInit)
        {
            let source : any = this._map.getSource("weather");
            source.setData(geoJson);
        }
        else
        {
            this._sourceInit = true;
            this._map.addSource("weather", {
                type : "geojson",
                data : geoJson
            });
        }
    }

    public onMove(callBack : ()=>void) : void
    {
        this._map.on("move", callBack);
    }

    public getBounds() : mapboxgl.LngLatBounds
    {
        return this._map.getBounds();
    }

    public getZoom() : number
    {
        return this._map.getZoom();
    }

    public zoomIn() : void
    {
        this._map.zoomIn();
    }

    public zoomOut() : void
    {
        this._map.zoomOut();
    }

    public switchStyle(style : MapModes)
    {
        for (let layerId of this._layerIds)
        {
            this._map.removeLayer(layerId);
        }
        this._layerIds.length = 0;

        switch(style)
        {
            case MapModes.Cluster:
                this.createClusterLayers();
                this.enableClusterListeners();
                this._currentMode = style;
                break;
            case MapModes.Heatmap:
                this.disableClusterListeners();
                this.createHeatmapLayers();
                this._currentMode = style;
                break;
        }
    }

    private enableClusterListeners() : void
    {
        if (this._currentMode === MapModes.Heatmap || this._currentMode === undefined)
        {
            this._map.on("click", "clusters", this.showClusterPopup);
            this._map.on("click", "unclustered-points", this.showClusterPopup);
            this._map.on("mouseenter", "clusters", this.setCursorToPointer);
            this._map.on("mouseenter", "unclustered-points", this.setCursorToPointer);
            this._map.on("mouseleave", "clusters", this.setCursorToNormal);
            this._map.on("mouseleave", "unclustered-points", this.setCursorToNormal);
        }
    }

    private disableClusterListeners() : void
    {
        if (this._currentMode === MapModes.Cluster)
        {
            this._map.off("click", "clusters", this.showClusterPopup);
            this._map.off("click", "unclustered-points", this.showClusterPopup);
            this._map.off("mouseenter", "clusters", this.setCursorToPointer);
            this._map.off("mouseenter", "unclustered-points", this.setCursorToPointer);
            this._map.off("mouseleave", "clusters", this.setCursorToNormal);
            this._map.off("mouseleave", "unclustered-points", this.setCursorToNormal);
        }
    }

    private setCursorToPointer() : void
    {
        this._map.getCanvas().style.cursor = 'pointer';
    }

    private setCursorToNormal() : void
    {
        this._map.getCanvas().style.cursor = '';
    }

    private showClusterPopup(e : any) : void
    {
        new mapboxgl.Popup()
            .setLngLat(e.features[0].geometry.coordinates)
            .setHTML(this.formatClusterInformation(e.features[0].properties))
            .addTo(this._map);
    }

    private formatClusterInformation(clusterProperties : any) : string
    {
        let header : string = "<strong>Cluster Information</strong>";
        let textStart : string = "<p>";
        let stationCount : string = "Number of stations: " + clusterProperties.pointCount + "</br>";
        let avgSnowDepth : string = "Average snow depth (cm): " + clusterProperties.snowDepth + "</br>";
        let minSnowDepth : string = "Minimum snow depth (cm): " + clusterProperties.minSnowDepth + "</br>";
        let maxSnowDepth : string = "Maximum snow depth (cm): " + clusterProperties.maxSnowDepth + "</br>";
        let textEnd : string = "</p>";
        return header + textStart + stationCount + avgSnowDepth + minSnowDepth + maxSnowDepth + textEnd;
    }

    private getDataForInit() : GeoJson.FeatureCollection<GeoJson.Point>
    {
        let emptyCollection : GeoJson.FeatureCollection<GeoJson.Point> = {
            type : "FeatureCollection",
            features : []
        };
        return emptyCollection;
    }

    private addLayer(layer : mapboxgl.Layer, before? : string) : void
    {
        this._layerIds.push(layer.id);
        this._map.addLayer(layer, before);
    }

    private createClusterLayers() : void
    {
        this.addLayer({
            id: "clusters",
            type: "circle",
            source: "weather",
            filter: ["==", "isCluster", true],
            paint: {
                "circle-color": {
                    property: "snowDepth",
                    type: "interval",
                    stops: Config.getMapboxColorInterval()
                },
                "circle-radius": {
                    property: "pointCount",
                    type: "interval",
                    stops: [
                        [0, 15],
                        [100, 17],
                        [300, 20],
                        [1000, 30]
                    ]
                }
            }
        });

        this.addLayer({
            id: "cluster-count",
            type: "symbol",
            source: "weather",
            filter: ["==", "isCluster", true],
            layout: {
                "text-field": "{snowDepthFloor}",
                "text-font": ["Arial Unicode MS Bold"],
                "text-size": 12,
                
            },
            paint : {
                "text-color" : "#000",
                "text-halo-color" : "rgba(255, 255, 255, 0.7)",
                "text-halo-width" : 1
            }
        });

        this.addLayer({
            id: "unclustered-points",
            type: "circle",
            source: "weather",
            filter: ["==", "isCluster", false],
            paint: {
                "circle-color": {
                    property: "snowDepth",
                    type: "interval",
                    stops: Config.getMapboxColorInterval()
                },
                "circle-radius": 6,
                "circle-stroke-width": 1,
                "circle-stroke-color": "#fff"
            }
        });
    }

    private createHeatmapLayers()
    {
        let layerColours = Config.getMapboxColorInterval();

        for (let i = 0; i < layerColours.length; i++)
        {
            this.addLayer({
                "id": "cluster-" + i,
                "type": "circle",
                "source": "weather",
                "paint": {
                    "circle-color": layerColours[i][1],
                    "circle-radius": 70,
                    "circle-blur": 1,
                    "circle-opacity": 1.0
                },
                "filter": i === layerColours.length - 1 ?
                    [">=", "snowDepth", layerColours[i][0]] :
                    ["all",
                        [">=", "snowDepth", layerColours[i][0]],
                        ["<", "snowDepth", layerColours[i + 1][0]]]
            }, 'admin_level_3');
        }

        this.addLayer({
            "id": "unclustered-points-heat",
            "type": "circle",
            "source": "weather",
            "paint": {
                "circle-color": {
                    property: "snowDepth",
                    type: "interval",
                    stops: layerColours
                },
                "circle-radius": 20,
                "circle-blur": 1,
                "circle-opacity": 1.0
            },
            "filter": ["==", "isCluster", false]
        }, 'admin_level_3');
    }
}

export enum MapModes
{
    Cluster,
    Heatmap
}