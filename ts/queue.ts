export class Queue<T>
{
    private _storage : T[];

    constructor()
    {
        this._storage = new Array();
    }

    public push(value : T)
    {
        this._storage.push(value);
    }

    public pop() : T | undefined
    {
        return this._storage.shift();
    }

    public isEmpty() : boolean
    {
        return this._storage.length == 0;
    }
}