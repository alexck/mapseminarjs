import * as GeoJson from "@types/geojson";

export class TestHelper
{
    public static getFeatureForProperty(property : any) : GeoJson.Feature<GeoJson.Point>
    {
        let testGeometry : GeoJSON.Point = {
            type : "Point",
            coordinates : [5, 10]
        };

        let feature : GeoJSON.Feature<GeoJSON.Point> = {
            type: "Feature",
            geometry: testGeometry,
            properties: property
        };
        return feature;
    }   
}