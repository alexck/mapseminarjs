import mapboxgl = require('mapbox-gl');
import { Vector2 } from "./vectorTwo";
import { MapManager } from "./mapManager";
import { ChartManager } from "./chartManager";
import { ElementManager } from "./elementManager";
import { ClusterUtil } from "./clusterUtil";
import * as GeoJson from "@types/geojson";

export class RectangleSelector
{
    private _mapManager : MapManager;
    private _chartManager : ChartManager;
    private _elementManager : ElementManager;
    private _canvasContainer : HTMLElement;
    private _boxElement : HTMLElement;
    private _dragStart : Vector2;
    private _dragCurrent : Vector2;
    private _selectActive : boolean;

    constructor(mapManager : MapManager, chartManager : ChartManager, elementManager : ElementManager)
    {
        this._mapManager = mapManager;
        this._chartManager = chartManager;
        this._elementManager = elementManager;
        this._canvasContainer = this._mapManager.getMapCanvas();
        this._canvasContainer.addEventListener("mousedown", this.mapCanvasMouseDown, true);
        this._selectActive = false;

        /*
        this._elementManager.onRectSelectButtonClick(() => {
            this._selectActive = true;
        });
        */
    }

    private mapCanvasMouseDown = (ev : MouseEvent) =>
    {
        if (!(this._selectActive && ev.button === 0))
        {
            return;
        }
        this._mapManager.disableDragPan();
        document.addEventListener("mousemove", this.documentMouseMove);
        document.addEventListener("mouseup", this.documentMouseUp);

        this._dragStart = this.mouseEventToPoint(ev);
    }

    private documentMouseMove = (ev : MouseEvent) =>
    {
        this._dragCurrent = this.mouseEventToPoint(ev);

        if (!this._boxElement)
        {
            this._boxElement = document.createElement("div");
            this._boxElement.classList.add("selectBox");
            this._canvasContainer.appendChild(this._boxElement);
        }

        let minX = Math.min(this._dragStart.xPos, this._dragCurrent.xPos);
        let maxX = Math.max(this._dragStart.xPos, this._dragCurrent.xPos);
        let minY = Math.min(this._dragStart.yPos, this._dragCurrent.yPos);
        let maxY = Math.max(this._dragStart.yPos, this._dragCurrent.yPos);

        let pos = "translate(" + minX + "px," + minY + "px)";
        this._boxElement.style.transform = pos;
        this._boxElement.style.webkitTransform = pos;
        this._boxElement.style.width = maxX - minX + "px";
        this._boxElement.style.height = maxY - minY + "px";
    }

    private documentMouseUp = (ev : MouseEvent) =>
    {
        document.removeEventListener("mousemove", this.documentMouseMove);
        document.removeEventListener("mouseup", this.documentMouseUp);

        let dragEndPoint = this.mouseEventToPoint(ev);
        let features = this._mapManager.queryMapByBoundingBox([this._dragStart.convertToArray(), dragEndPoint.convertToArray()]);
        let duplicateFreeClusters = ClusterUtil.removeDuplicateClusters(features);
        this._chartManager.updateChartData(duplicateFreeClusters);

        this._boxElement.parentNode.removeChild(this._boxElement);
        this._boxElement = null;
        this._selectActive = false;
        this._elementManager.showInfoBox();
        this._mapManager.enableDragPan();
    }

    private mouseEventToPoint(ev : MouseEvent) : Vector2
    {
        let rect = this._canvasContainer.getBoundingClientRect();
        let posX = ev.clientX - rect.left - this._canvasContainer.clientLeft;
        let posY = ev.clientY - rect.top - this._canvasContainer.clientTop;
        return new Vector2(posX, posY);
    }
}