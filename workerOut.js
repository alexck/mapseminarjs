/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Die Methoden longitudeToMercator, latitudeToMercator, mercatorToLongitude, mercatorToLatitude
 * enstammen dem Code der Supercluster Library
 * Authoren: Vladimir Agafonkin, Andrew Harvey, Github Nutzer: jingsam
 * Source Code online unter: https://github.com/mapbox/supercluster/blob/master/index.js
 */
function longitudeToMercator(longitude) {
    return (longitude / 360) + 0.5;
}
exports.longitudeToMercator = longitudeToMercator;
function latitudeToMercator(latitude) {
    var sin = Math.sin(latitude * Math.PI / 180);
    var y = (0.5 - 0.25 * Math.log((1 + sin) / (1 - sin)) / Math.PI);
    return y < 0 ? 0 : y > 1 ? 1 : y;
}
exports.latitudeToMercator = latitudeToMercator;
function mercatorToLongitude(mercatorX) {
    return (mercatorX - 0.5) * 360;
}
exports.mercatorToLongitude = mercatorToLongitude;
function mercatorToLatitude(mercatorY) {
    var y = (180 - mercatorY * 360) * Math.PI / 180;
    return 360 * Math.atan(Math.exp(y)) / Math.PI - 90;
}
exports.mercatorToLatitude = mercatorToLatitude;
function mapboxBoundsToMercator(boundingBox) {
    var minX = longitudeToMercator(boundingBox.southWestLng);
    var maxX = longitudeToMercator(boundingBox.northEastLng);
    var minY = latitudeToMercator(boundingBox.northEastLat);
    var maxY = latitudeToMercator(boundingBox.southWestLat);
    var mercatorBoundingBox = {
        minXPos: minX,
        maxXPos: maxX,
        minYPos: minY,
        maxYPos: maxY
    };
    return mercatorBoundingBox;
}
exports.mapboxBoundsToMercator = mapboxBoundsToMercator;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Util = __webpack_require__(0);
var Vector2 = (function () {
    function Vector2(xPos, yPos) {
        this._xPos = xPos;
        this._yPos = yPos;
    }
    Object.defineProperty(Vector2.prototype, "xPos", {
        get: function () {
            return this._xPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "yPos", {
        get: function () {
            return this._yPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "longitudeFromMercator", {
        get: function () {
            return Util.mercatorToLongitude(this._xPos);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "latitudeFromMercator", {
        get: function () {
            return Util.mercatorToLatitude(this._yPos);
        },
        enumerable: true,
        configurable: true
    });
    Vector2.prototype.convertToArray = function () {
        return [this._xPos, this._yPos];
    };
    Vector2.createMercatorVector = function (longitude, latitude) {
        return new Vector2(Util.longitudeToMercator(longitude), Util.latitudeToMercator(latitude));
    };
    return Vector2;
}());
exports.Vector2 = Vector2;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var kdbush = __webpack_require__(5);
var cluster_1 = __webpack_require__(9);
var Util = __webpack_require__(0);
var parser_1 = __webpack_require__(12);
var vectorTwo_1 = __webpack_require__(1);
/*
 * Dieser Code basiert auf Code der Supercluster Library
 * Authoren: Vladimir Agafonkin, Andrew Harvey, Github Nutzer: jingsam
 * Source Code online unter: https://github.com/mapbox/supercluster/blob/master/index.js
 * Zuletzt gesichtet: 02.07.2017
 */
var SuperCluster = (function () {
    function SuperCluster(minZoom, maxZoom, radius, extend, nodeSize, shouldAccumulate, reduceFunctions) {
        if (minZoom === void 0) { minZoom = 0; }
        if (maxZoom === void 0) { maxZoom = 16; }
        if (radius === void 0) { radius = 50; }
        if (extend === void 0) { extend = 512; }
        if (nodeSize === void 0) { nodeSize = 64; }
        if (shouldAccumulate === void 0) { shouldAccumulate = false; }
        this._minZoom = minZoom;
        this._maxZoom = maxZoom;
        this._radius = radius;
        this._extend = extend;
        this._nodeSize = nodeSize;
        this._clusterTree = new Array(maxZoom + 1);
        this._shouldAccumulate = shouldAccumulate;
        this._reduceFunctions = reduceFunctions;
    }
    SuperCluster.prototype.setRadius = function (radius) {
        this._radius = radius;
        this.load(this._points);
    };
    SuperCluster.prototype.load = function (points) {
        this._points = points;
        var clusters = new Array();
        for (var i = 0; i < points.length; i++) {
            if (this._shouldAccumulate) {
                clusters.push(cluster_1.createClusterByPoint(points[i], i, this._reduceFunctions.accInitial(points[i].properties)));
            }
            else {
                clusters.push(cluster_1.createClusterByPoint(points[i], i));
            }
        }
        var getClusterXPos = function (cluster) {
            return cluster.pos.xPos;
        };
        var getClusterYPos = function (cluster) {
            return cluster.pos.yPos;
        };
        this._clusterTree[this._maxZoom + 1] = kdbush(clusters, getClusterXPos, getClusterYPos, this._nodeSize);
        for (var i = this._maxZoom; i >= this._minZoom; i--) {
            clusters = this.cluster(clusters, i);
            this._clusterTree[i] = kdbush(clusters, getClusterXPos, getClusterYPos, this._nodeSize);
        }
    };
    SuperCluster.prototype.getClustersGeoJson = function (boundingBox, zoom) {
        return parser_1.JsonParser.getGeoJsonFromClusters(this.getClusters(boundingBox, zoom));
    };
    SuperCluster.prototype.getClusters = function (boundingBox, zoom) {
        if (!this._points) {
            throw new TypeError("Clusters not loaded yet");
        }
        var tree = this._clusterTree[this.clampZoom(zoom)];
        var mercatorBoundingBox = Util.mapboxBoundsToMercator(boundingBox);
        var ids = tree.range(mercatorBoundingBox.minXPos, mercatorBoundingBox.minYPos, mercatorBoundingBox.maxXPos, mercatorBoundingBox.maxYPos);
        var clusters = new Array();
        for (var _i = 0, ids_1 = ids; _i < ids_1.length; _i++) {
            var currentId = ids_1[_i];
            var currentCluster = tree.points[currentId];
            clusters.push(currentCluster);
        }
        return clusters;
    };
    SuperCluster.prototype.cluster = function (currentClusters, clusterZoomLevel) {
        var clusters = new Array();
        var r = this._radius / (this._extend * Math.pow(2, clusterZoomLevel));
        for (var i = 0; i < currentClusters.length; i++) {
            var point = currentClusters[i];
            if (point.zoom <= clusterZoomLevel) {
                continue;
            }
            point.zoom = clusterZoomLevel;
            var tree = this._clusterTree[clusterZoomLevel + 1];
            var neighborIds = tree.within(point.pos.xPos, point.pos.yPos, r);
            var numPoints = point.numberOfPoints;
            var wX = point.pos.xPos * numPoints;
            var wY = point.pos.yPos * numPoints;
            var initialGeoProp = void 0;
            if (this._shouldAccumulate) {
                initialGeoProp = point.clusterProperties.reducedProperties;
            }
            for (var _i = 0, neighborIds_1 = neighborIds; _i < neighborIds_1.length; _i++) {
                var currentNeighborId = neighborIds_1[_i];
                var currentNeighbor = tree.points[currentNeighborId];
                if (currentNeighbor.zoom <= clusterZoomLevel) {
                    continue;
                }
                currentNeighbor.zoom = clusterZoomLevel;
                var neighborNumPoints = currentNeighbor.numberOfPoints;
                wX += currentNeighbor.pos.xPos * neighborNumPoints;
                wY += currentNeighbor.pos.yPos * neighborNumPoints;
                numPoints += neighborNumPoints;
                currentNeighbor.parentId = i;
                if (this._shouldAccumulate) {
                    initialGeoProp = this._reduceFunctions.accFunction(currentNeighbor.clusterProperties.reducedProperties, initialGeoProp);
                }
            }
            if (numPoints === 1) {
                clusters.push(point);
            }
            else {
                point.parentId = i;
                if (this._shouldAccumulate) {
                    initialGeoProp = this._reduceFunctions.accFinalize(neighborIds.length, initialGeoProp);
                    clusters.push(cluster_1.createCluster(new vectorTwo_1.Vector2(wX / numPoints, wY / numPoints), i, numPoints, initialGeoProp));
                }
                else {
                    clusters.push(cluster_1.createCluster(new vectorTwo_1.Vector2(wX / numPoints, wY / numPoints), i, numPoints));
                }
            }
        }
        return clusters;
    };
    SuperCluster.prototype.clampZoom = function (zoom) {
        return Math.round(Math.max(this._minZoom, Math.min(zoom, this._maxZoom + 1)));
    };
    return SuperCluster;
}());
exports.SuperCluster = SuperCluster;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Config = (function () {
    function Config() {
    }
    Config.getDepthInterval = function () {
        return this.depthInterval;
    };
    Config.getColorInterval = function () {
        return this.colorInterval;
    };
    Config.getDatasetYear = function () {
        return this.datasetYear;
    };
    Config.getClusterMapRadius = function () {
        return this.clusterMapRadius;
    };
    Config.getHeatMapRadius = function () {
        return this.heatMapRadius;
    };
    Config.getDefaultPlaybackSpeed = function () {
        return this.defaultPlaybackSpeed;
    };
    Config.getMapboxColorInterval = function () {
        if (this.depthInterval.length !== this.colorInterval.length) {
            throw new RangeError("depthInterval and colorInterval have different length");
        }
        var returnArray = new Array();
        for (var i = 0; i < this.depthInterval.length; i++) {
            var newEntry = [this.depthInterval[i], this.colorInterval[i]];
            returnArray.push(newEntry);
        }
        return returnArray;
    };
    Config.getDepthIntervalPrettyPrint = function () {
        var depthIntervalTextual = this.depthInterval.map(String);
        var retArray = new Array(depthIntervalTextual.length);
        for (var i = 0; i < depthIntervalTextual.length - 1; i++) {
            retArray[i] = depthIntervalTextual[i] + "-" + depthIntervalTextual[i + 1];
        }
        retArray[depthIntervalTextual.length - 1] = depthIntervalTextual[depthIntervalTextual.length - 1] + "+";
        return retArray;
    };
    return Config;
}());
Config.depthInterval = [0, 2, 40, 100];
Config.colorInterval = ["#009933", "#00ccff", "#0066ff", "#8c1aff"];
Config.datasetYear = 2016;
Config.clusterMapRadius = 50;
Config.heatMapRadius = 20;
Config.defaultPlaybackSpeed = 900;
exports.Config = Config;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var MessageType;
(function (MessageType) {
    MessageType[MessageType["LoadData"] = 0] = "LoadData";
    MessageType[MessageType["RequestData"] = 1] = "RequestData";
    MessageType[MessageType["UpdateClusterRadius"] = 2] = "UpdateClusterRadius";
    MessageType[MessageType["WorkerGeoDataResponse"] = 3] = "WorkerGeoDataResponse";
})(MessageType = exports.MessageType || (exports.MessageType = {}));


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var sort = __webpack_require__(7);
var range = __webpack_require__(6);
var within = __webpack_require__(8);

module.exports = kdbush;

function kdbush(points, getX, getY, nodeSize, ArrayType) {
    return new KDBush(points, getX, getY, nodeSize, ArrayType);
}

function KDBush(points, getX, getY, nodeSize, ArrayType) {
    getX = getX || defaultGetX;
    getY = getY || defaultGetY;
    ArrayType = ArrayType || Array;

    this.nodeSize = nodeSize || 64;
    this.points = points;

    this.ids = new ArrayType(points.length);
    this.coords = new ArrayType(points.length * 2);

    for (var i = 0; i < points.length; i++) {
        this.ids[i] = i;
        this.coords[2 * i] = getX(points[i]);
        this.coords[2 * i + 1] = getY(points[i]);
    }

    sort(this.ids, this.coords, this.nodeSize, 0, this.ids.length - 1, 0);
}

KDBush.prototype = {
    range: function (minX, minY, maxX, maxY) {
        return range(this.ids, this.coords, minX, minY, maxX, maxY, this.nodeSize);
    },

    within: function (x, y, r) {
        return within(this.ids, this.coords, x, y, r, this.nodeSize);
    }
};

function defaultGetX(p) { return p[0]; }
function defaultGetY(p) { return p[1]; }


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = range;

function range(ids, coords, minX, minY, maxX, maxY, nodeSize) {
    var stack = [0, ids.length - 1, 0];
    var result = [];
    var x, y;

    while (stack.length) {
        var axis = stack.pop();
        var right = stack.pop();
        var left = stack.pop();

        if (right - left <= nodeSize) {
            for (var i = left; i <= right; i++) {
                x = coords[2 * i];
                y = coords[2 * i + 1];
                if (x >= minX && x <= maxX && y >= minY && y <= maxY) result.push(ids[i]);
            }
            continue;
        }

        var m = Math.floor((left + right) / 2);

        x = coords[2 * m];
        y = coords[2 * m + 1];

        if (x >= minX && x <= maxX && y >= minY && y <= maxY) result.push(ids[m]);

        var nextAxis = (axis + 1) % 2;

        if (axis === 0 ? minX <= x : minY <= y) {
            stack.push(left);
            stack.push(m - 1);
            stack.push(nextAxis);
        }
        if (axis === 0 ? maxX >= x : maxY >= y) {
            stack.push(m + 1);
            stack.push(right);
            stack.push(nextAxis);
        }
    }

    return result;
}


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = sortKD;

function sortKD(ids, coords, nodeSize, left, right, depth) {
    if (right - left <= nodeSize) return;

    var m = Math.floor((left + right) / 2);

    select(ids, coords, m, left, right, depth % 2);

    sortKD(ids, coords, nodeSize, left, m - 1, depth + 1);
    sortKD(ids, coords, nodeSize, m + 1, right, depth + 1);
}

function select(ids, coords, k, left, right, inc) {

    while (right > left) {
        if (right - left > 600) {
            var n = right - left + 1;
            var m = k - left + 1;
            var z = Math.log(n);
            var s = 0.5 * Math.exp(2 * z / 3);
            var sd = 0.5 * Math.sqrt(z * s * (n - s) / n) * (m - n / 2 < 0 ? -1 : 1);
            var newLeft = Math.max(left, Math.floor(k - m * s / n + sd));
            var newRight = Math.min(right, Math.floor(k + (n - m) * s / n + sd));
            select(ids, coords, k, newLeft, newRight, inc);
        }

        var t = coords[2 * k + inc];
        var i = left;
        var j = right;

        swapItem(ids, coords, left, k);
        if (coords[2 * right + inc] > t) swapItem(ids, coords, left, right);

        while (i < j) {
            swapItem(ids, coords, i, j);
            i++;
            j--;
            while (coords[2 * i + inc] < t) i++;
            while (coords[2 * j + inc] > t) j--;
        }

        if (coords[2 * left + inc] === t) swapItem(ids, coords, left, j);
        else {
            j++;
            swapItem(ids, coords, j, right);
        }

        if (j <= k) left = j + 1;
        if (k <= j) right = j - 1;
    }
}

function swapItem(ids, coords, i, j) {
    swap(ids, i, j);
    swap(coords, 2 * i, 2 * j);
    swap(coords, 2 * i + 1, 2 * j + 1);
}

function swap(arr, i, j) {
    var tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
}


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = within;

function within(ids, coords, qx, qy, r, nodeSize) {
    var stack = [0, ids.length - 1, 0];
    var result = [];
    var r2 = r * r;

    while (stack.length) {
        var axis = stack.pop();
        var right = stack.pop();
        var left = stack.pop();

        if (right - left <= nodeSize) {
            for (var i = left; i <= right; i++) {
                if (sqDist(coords[2 * i], coords[2 * i + 1], qx, qy) <= r2) result.push(ids[i]);
            }
            continue;
        }

        var m = Math.floor((left + right) / 2);

        var x = coords[2 * m];
        var y = coords[2 * m + 1];

        if (sqDist(x, y, qx, qy) <= r2) result.push(ids[m]);

        var nextAxis = (axis + 1) % 2;

        if (axis === 0 ? qx - r <= x : qy - r <= y) {
            stack.push(left);
            stack.push(m - 1);
            stack.push(nextAxis);
        }
        if (axis === 0 ? qx + r >= x : qy + r >= y) {
            stack.push(m + 1);
            stack.push(right);
            stack.push(nextAxis);
        }
    }

    return result;
}

function sqDist(ax, ay, bx, by) {
    var dx = ax - bx;
    var dy = ay - by;
    return dx * dx + dy * dy;
}


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var vectorTwo_1 = __webpack_require__(1);
/*
 * Dieser Code basiert auf Code der Supercluster Library
 * Authoren: Vladimir Agafonkin, Andrew Harvey, Github Nutzer: jingsam
 * Source Code online unter: https://github.com/mapbox/supercluster/blob/master/index.js
 * Zuletzt gesichtet: 02.07.2017
 */
var Cluster = (function () {
    function Cluster(pos, zoom, id, parentId, numberOfPoints, isCluster, originalProperties, reducedProperties) {
        this._pos = pos;
        this._zoom = zoom;
        this._id = id;
        this._parentId = parentId;
        this._numberOfPoints = numberOfPoints;
        this._isCluster = isCluster;
        this._reducedProperties = reducedProperties;
        this._originalProperties = originalProperties;
    }
    Object.defineProperty(Cluster.prototype, "pos", {
        get: function () {
            return this._pos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cluster.prototype, "zoom", {
        get: function () {
            return this._zoom;
        },
        set: function (newZoom) {
            this._zoom = newZoom;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cluster.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cluster.prototype, "parentId", {
        get: function () {
            return this._parentId;
        },
        set: function (newParentId) {
            this._parentId = newParentId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cluster.prototype, "numberOfPoints", {
        get: function () {
            return this._numberOfPoints;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cluster.prototype, "clusterProperties", {
        get: function () {
            var abbrev = this._numberOfPoints >= 10000 ? Math.round(this._numberOfPoints / 1000) + "k" :
                this._numberOfPoints >= 1000 ? (Math.round(this._numberOfPoints / 100) / 10) + "k" :
                    this._numberOfPoints;
            var props = {
                clusterId: this._id,
                originalProperties: this._originalProperties,
                reducedProperties: this._reducedProperties,
                isCluster: this._isCluster,
                pointCount: this._numberOfPoints,
                pointCountAbbreviated: abbrev.toString()
            };
            return props;
        },
        enumerable: true,
        configurable: true
    });
    Cluster.prototype.getClusterGeoJson = function () {
        var feature = {
            type: "Feature",
            properties: Object.assign(this.clusterProperties, this._reducedProperties),
            geometry: {
                type: "Point",
                coordinates: [this._pos.longitudeFromMercator, this._pos.latitudeFromMercator]
            }
        };
        return feature;
    };
    return Cluster;
}());
exports.Cluster = Cluster;
function createClusterByPoint(point, id, reducedProperties) {
    var coordinates = point.geometry.coordinates;
    var pos = vectorTwo_1.Vector2.createMercatorVector(coordinates[0], coordinates[1]);
    if (reducedProperties) {
        return new Cluster(pos, Infinity, id, -1, 1, false, point.properties, reducedProperties);
    }
    else {
        return new Cluster(pos, Infinity, id, -1, 1, false, point.properties);
    }
}
exports.createClusterByPoint = createClusterByPoint;
function createCluster(pos, identifier, numPoints, reducedProperties) {
    if (reducedProperties === void 0) { reducedProperties = {}; }
    return new Cluster(pos, Infinity, identifier, -1, numPoints, true, {}, reducedProperties);
}
exports.createCluster = createCluster;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var clustering_1 = __webpack_require__(2);
var workerMessage_1 = __webpack_require__(4);
var config_1 = __webpack_require__(3);
init();
function init() {
    var reduceFunctions = {
        accInitial: function (origProps) {
            var dataAvailable = origProps.info.snowDepthAvailable;
            var snowDepthData = parseFloat(origProps.info.snowDepth);
            var snowDepthInCm = snowDepthData / 10;
            var numberOfValidItems = dataAvailable ? 1 : 0;
            return {
                numberOfValidPoints: numberOfValidItems,
                snowDepth: snowDepthInCm,
                minSnowDepth: snowDepthInCm,
                maxSnowDepth: snowDepthInCm
            };
        },
        accFunction: function (geoProps, currentProps) {
            var snowDepthData = geoProps.snowDepth + currentProps.snowDepth;
            var minSnowDepthData = Math.min(geoProps.minSnowDepth, currentProps.minSnowDepth);
            var maxSnowDepthData = Math.max(geoProps.maxSnowDepth, currentProps.maxSnowDepth);
            var numberOfValidItems = geoProps.numberOfValidPoints + currentProps.numberOfValidPoints;
            var snowDepthFloor = Math.floor(snowDepthData);
            return {
                numberOfValidPoints: numberOfValidItems,
                snowDepth: snowDepthData,
                minSnowDepth: minSnowDepthData,
                maxSnowDepth: maxSnowDepthData
            };
        },
        accFinalize: function (numberOfNeighbors, geoProps) {
            var numberOfValidItems = geoProps.numberOfValidPoints;
            var snowDepthData = 0;
            if (numberOfValidItems > 0) {
                numberOfValidItems = 1;
                snowDepthData = parseFloat((geoProps.snowDepth / geoProps.numberOfValidPoints).toFixed(2));
            }
            return {
                numberOfValidPoints: numberOfValidItems,
                snowDepth: snowDepthData,
                snowDepthFloor: Math.floor(snowDepthData),
                minSnowDepth: geoProps.minSnowDepth,
                maxSnowDepth: geoProps.maxSnowDepth
            };
        }
    };
    var supercluster = new clustering_1.SuperCluster(0, 16, config_1.Config.getClusterMapRadius(), 512, 64, true, reduceFunctions);
    addEventListener('message', function (message) {
        var convertedMessage = message.data;
        switch (convertedMessage.type) {
            case workerMessage_1.MessageType.LoadData:
                supercluster.load(convertedMessage.parameters);
                break;
            case workerMessage_1.MessageType.UpdateClusterRadius:
                supercluster.setRadius(convertedMessage.parameters);
                break;
            case workerMessage_1.MessageType.RequestData:
                try {
                    var requestParameters = convertedMessage.parameters;
                    var calculatedGeoJson = supercluster.getClustersGeoJson(requestParameters.boundingBox, requestParameters.zoom);
                    var response = {
                        type: workerMessage_1.MessageType.WorkerGeoDataResponse,
                        parameters: calculatedGeoJson
                    };
                    postMessage(JSON.stringify(response));
                    break;
                }
                catch (e) {
                    console.log("Tried to get cluster data before initialization");
                }
        }
    });
}


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Optional = (function () {
    function Optional(isPresent, dataItem) {
        this.dataItem_ = dataItem;
        this.isPresent_ = isPresent;
    }
    Object.defineProperty(Optional.prototype, "isPresent", {
        get: function () {
            return this.isPresent_;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Optional.prototype, "dataItem", {
        get: function () {
            return this.dataItem_;
        },
        enumerable: true,
        configurable: true
    });
    Optional.empty = function () {
        return new Optional(false, undefined);
    };
    Optional.of = function (dataItem) {
        return new Optional(true, dataItem);
    };
    return Optional;
}());
exports.Optional = Optional;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var optional_1 = __webpack_require__(11);
var JsonParser = (function () {
    function JsonParser() {
    }
    JsonParser.parseGeoJson = function (geoJson) {
        var parsedJsonObject = JSON.parse(geoJson);
        if (this.isValidGeoJson(parsedJsonObject)) {
            return optional_1.Optional.of(parsedJsonObject);
        }
        else {
            return optional_1.Optional.empty();
        }
    };
    JsonParser.parseDateJson = function (dateJson) {
        var parsedJson = JSON.parse(dateJson);
        if (this.isValidDateJson(parsedJson)) {
            return optional_1.Optional.of(parsedJson);
        }
        else {
            return optional_1.Optional.empty();
        }
    };
    JsonParser.getGeoJsonFromClusters = function (clusters) {
        var geoJsonFeatures = new Array();
        for (var _i = 0, clusters_1 = clusters; _i < clusters_1.length; _i++) {
            var currentCluster = clusters_1[_i];
            geoJsonFeatures.push(currentCluster.getClusterGeoJson());
        }
        var collection = {
            type: "FeatureCollection",
            features: geoJsonFeatures
        };
        return collection;
    };
    JsonParser.isValidGeoJson = function (geoJson) {
        return geoJson.type !== undefined;
    };
    JsonParser.isValidDateJson = function (dateJson) {
        return dateJson.dates !== undefined;
    };
    return JsonParser;
}());
exports.JsonParser = JsonParser;


/***/ })
/******/ ]);
//# sourceMappingURL=workerOut.js.map