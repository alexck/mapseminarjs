import * as GeoJson from "@types/geojson";

export class ClusterUtil
{
    public static removeDuplicateClusters(data : GeoJSON.Feature<GeoJSON.GeometryObject>[]) : GeoJSON.Feature<GeoJson.GeometryObject>[]
    {
        let retClusters : GeoJson.Feature<GeoJson.GeometryObject>[] = new Array();
        let detectedClusterIds : number[] = new Array();

        for (let feature of data)
        {
            let currentClusterId = (<any>feature.properties).clusterId;

            if (detectedClusterIds.indexOf(currentClusterId) === -1)
            {
                detectedClusterIds.push(currentClusterId);
                retClusters.push(feature);
            }
        }
        return retClusters;
    }
}