import * as GeoJson from "@types/geojson";
import * as Util from "./helper";
import { Vector2 } from "./vectorTwo";

/*
 * Dieser Code basiert auf Code der Supercluster Library
 * Authoren: Vladimir Agafonkin, Andrew Harvey, Github Nutzer: jingsam
 * Source Code online unter: https://github.com/mapbox/supercluster/blob/master/index.js
 * Zuletzt gesichtet: 02.07.2017 
 */
export class Cluster
{
    private _pos : Vector2;
    private _zoom : number;
    private _id : number;
    private _parentId : number;
    private _numberOfPoints : number;
    private _isCluster : boolean;
    private _originalProperties : any;
    private _reducedProperties : any;

    constructor(pos : Vector2, zoom : number, id : number, parentId : number,
                numberOfPoints : number, isCluster : boolean, originalProperties : any, reducedProperties? : any)
    {
        this._pos = pos;
        this._zoom = zoom;
        this._id = id;
        this._parentId = parentId;
        this._numberOfPoints = numberOfPoints;
        this._isCluster = isCluster;
        this._reducedProperties = reducedProperties;
        this._originalProperties = originalProperties;
    }

    get pos() : Vector2
    {
        return this._pos;
    }

    get zoom() : number
    {
        return this._zoom;
    }

    set zoom(newZoom : number)
    {
        this._zoom = newZoom;
    }

    get id() : number
    {
        return this._id;
    }

    get parentId() : number
    {
        return this._parentId;
    }

    set parentId(newParentId : number)
    {
        this._parentId = newParentId;
    }

    get numberOfPoints() : number
    {
        return this._numberOfPoints;
    }

    get clusterProperties() : IClusterProperties
    {
        let abbrev = this._numberOfPoints >= 10000 ? Math.round(this._numberOfPoints / 1000) + "k" :
                     this._numberOfPoints >= 1000 ? (Math.round(this._numberOfPoints / 100) / 10) + "k" :
                     this._numberOfPoints;
        let props : IClusterProperties = {
            clusterId : this._id,
            originalProperties : this._originalProperties,
            reducedProperties : this._reducedProperties,
            isCluster : this._isCluster,
            pointCount : this._numberOfPoints,
            pointCountAbbreviated : abbrev.toString()
        };
        return props;
    }

    public getClusterGeoJson() : GeoJSON.Feature<GeoJSON.Point>
    {
        let feature : GeoJson.Feature<GeoJson.Point> = {
        type : "Feature",
        properties : Object.assign(this.clusterProperties, this._reducedProperties),
        geometry : {
            type : "Point",
            coordinates : [this._pos.longitudeFromMercator, this._pos.latitudeFromMercator]
            }
        };
        return feature;
    }
}

export interface IClusterProperties
{
    clusterId : number;
    originalProperties : any;
    reducedProperties : any;
    isCluster : boolean;
    pointCount : number;
    pointCountAbbreviated : string;
}

export function createClusterByPoint(point : GeoJson.Feature<GeoJSON.Point>, id : number, reducedProperties? : any) : Cluster
{
    let coordinates : number[] = point.geometry.coordinates;
    let pos : Vector2 = Vector2.createMercatorVector(coordinates[0], coordinates[1]);
    
    if (reducedProperties)
    {
        return new Cluster(pos, Infinity, id, -1, 1, false, point.properties, reducedProperties);
    }
    else
    {
        return new Cluster(pos, Infinity, id, -1, 1, false, point.properties);
    }
}

export function createCluster(pos : Vector2, identifier : number, numPoints : number,
                              reducedProperties : any = {}) : Cluster
{
    return new Cluster(pos, Infinity, identifier, -1, numPoints, true, {}, reducedProperties);
}
