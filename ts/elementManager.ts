import { Config } from "./config";

export class ElementManager
{
    private _infoboxClose : HTMLElement;
    private _infoBox : HTMLElement;
    private _legend : HTMLElement;
    private _playIcon : HTMLElement;
    private _pauseIcon : HTMLElement;
    private _playButton : HTMLElement;
    private _clusterButton : HTMLElement;
    private _heatmapButton : HTMLElement;
    //private _rectSelectButton : HTMLElement;
    private _zoomInButton : HTMLElement;
    private _zoomOutButton : HTMLElement;
    private _dateText : HTMLElement;
    private _timeRange : any;
    private _speedOneLabel : HTMLElement;
    private _speedTwoLabel : HTMLElement;
    private _speedThreeLabel : HTMLElement;
    private _radarCanvas : HTMLCanvasElement;
    private _barCanvas : HTMLCanvasElement;
    private _compCanvas : HTMLCanvasElement;

    constructor()
    {
        this.prepareUiElements();
        this.initializeOwnListeners();
        
        //this.initializeLegend();
    }

    public getRadarCanvasContext() : CanvasRenderingContext2D
    {
        return this._radarCanvas.getContext("2d");
    }

    public getBarCanvasContext() : CanvasRenderingContext2D
    {
        return this._barCanvas.getContext("2d");
    }

    public getCompCanvasContext() : CanvasRenderingContext2D
    {
        return this._compCanvas.getContext("2d");
    }

    public getTimeRangeValue() : number
    {
        return this._timeRange.value;
    }

    public setTimeRangeValue(newValue : number) : void
    {
        this._timeRange.value = newValue;
    }

    public setTimeRangeMaxValue(maxValue : number) : void
    {
        this._timeRange.max = maxValue;
    }

    public setDateText(newText : string) : void
    {
        this._dateText.innerHTML = newText;
    }

    public showPauseIcon() : void
    {
        this._pauseIcon.style.display = "inline";
    }

    public hidePauseIcon() : void
    {
        this._pauseIcon.style.display = "none";
    }

    public showPlayIcon() : void
    {
        this._playIcon.style.display = "inline";
    }

    public hidePlayIcon() : void
    {
        this._playIcon.style.display = "none";
    }

    public showInfoBox() : void
    {
        this._infoBox.classList.add("showMenu");
    }

    public hideInfoBox() : void
    {
        this._infoBox.classList.remove("showMenu");
    }

    public onPlayButtonClick(callback : () => any) : void
    {
        this._playButton.addEventListener("click", callback);
    }

    public onClusterButtonClick(callback : () => any) : void
    {
        this._clusterButton.addEventListener("click", callback);
    }

    public onHeatmapButtonClick(callback : () => any) : void
    {
        this._heatmapButton.addEventListener("click", callback);
    }

    /*
    public onRectSelectButtonClick(callback : () => any) : void
    {
        this._rectSelectButton.addEventListener("click", callback);
    }
    */

    public onZoomInButtonClick(callback : () => any) : void
    {
        this._zoomInButton.addEventListener("click", callback);
    }

    public onZoomOutButtonClick(callback : () => any) : void
    {
        this._zoomOutButton.addEventListener("click", callback);
    }

    public onTimeRangeChange(callback : () => any) : void
    {
        this._timeRange.addEventListener("change", callback);
    }

    public onSpeedOneLabelClick(callback : () => any) : void
    {
        this._speedOneLabel.addEventListener("click", callback);
    }

    public onSpeedTwoLabelClick(callback : () => any) : void
    {
        this._speedTwoLabel.addEventListener("click", callback);
    }

    public onSpeedThreeLabelClick(callback : () => any) : void
    {
        this._speedThreeLabel.addEventListener("click", callback);
    }

    private initializeLegend() : void
    {
        let elementTemplate : string = '<div><span style="background-color: {color}"></span>{depth}</div>';
        let depthInterval : string[] = Config.getDepthIntervalPrettyPrint();
        let colorInterval : string[] = Config.getColorInterval();

        for (let i = 0; i < depthInterval.length; i++)
        {
            let elementToInsert = elementTemplate.replace("{color}", colorInterval[i]).replace("{depth}", depthInterval[i]);
            this._legend.insertAdjacentHTML("beforeend", elementToInsert);
        }        
    }

    private initializeOwnListeners() : void
    {
       this._infoboxClose.addEventListener("click", () => {
            this.hideInfoBox();
        });

        this._speedOneLabel.addEventListener("click", () => 
        {
            this.switchActive(this._speedOneLabel, [this._speedTwoLabel, this._speedThreeLabel]);
        });

        this._speedTwoLabel.addEventListener("click", () =>
        {
            this.switchActive(this._speedTwoLabel, [this._speedOneLabel, this._speedThreeLabel]);
        });

        this._speedThreeLabel.addEventListener("click", () =>
        {
            this.switchActive(this._speedThreeLabel, [this._speedOneLabel, this._speedTwoLabel]);
        });
    }

    private switchActive(toActivate : HTMLElement, toDeactivate? : HTMLElement[])
    {
        toActivate.classList.add("active");

        if (toDeactivate)
        {
            for (let element of toDeactivate)
            {
                element.classList.remove("active");
            }
        }
    }

    private prepareUiElements() : void
    {
        this._infoboxClose = document.getElementById("infobox-close");
        this._infoBox = document.getElementById("infobox");
        this._legend = document.getElementById("legend");
        this._playIcon = document.getElementById("icon-play");
        this._pauseIcon = document.getElementById("icon-pause");
        this._playButton = document.getElementById("btnPlay");
        this._dateText = document.getElementById("date-text");
        this._clusterButton = document.getElementById("btnCluster");
        this._heatmapButton = document.getElementById("btnHeatmap");
        //this._rectSelectButton = document.getElementById("btnSelect");
        this._zoomInButton = document.getElementById("btnZoomIn")
        this._zoomOutButton = document.getElementById("btnZoomOut")
        this._timeRange = document.getElementById("rngTime");
        this._speedOneLabel = document.getElementById("speedOne");
        this._speedTwoLabel = document.getElementById("speedTwo");
        this._speedThreeLabel = document.getElementById("speedThree");
        
        this._radarCanvas = <HTMLCanvasElement>document.getElementById("radarChart");
        this._barCanvas = <HTMLCanvasElement>document.getElementById("barChart");
        this._compCanvas = <HTMLCanvasElement>document.getElementById("compChart");

        this.switchActive(this._speedOneLabel);
    }
}