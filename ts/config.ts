
export class Config
{
    private static depthInterval : number[] = [0, 2, 40, 100];
    private static colorInterval : string[] = ["#009933", "#00ccff", "#0066ff", "#8c1aff"];
    private static datasetYear : number = 2016;
    private static clusterMapRadius : number = 50;
    private static heatMapRadius : number = 20;
    private static defaultPlaybackSpeed : number = 900;

    public static getDepthInterval() : number[]
    {
        return this.depthInterval;
    }

    public static getColorInterval() : string[]
    {
        return this.colorInterval;
    }

    public static getDatasetYear() : number
    {
        return this.datasetYear;
    }

    public static getClusterMapRadius() : number
    {
        return this.clusterMapRadius;
    }

    public static getHeatMapRadius() : number
    {
        return this.heatMapRadius;
    }

    public static getDefaultPlaybackSpeed() : number
    {
        return this.defaultPlaybackSpeed;
    }

    public static getMapboxColorInterval() : (string|number)[][]
    {
        if (this.depthInterval.length !== this.colorInterval.length)
        {
            throw new RangeError("depthInterval and colorInterval have different length");
        }
        let returnArray : (string|number)[][] = new Array();

        for (let i = 0; i < this.depthInterval.length; i++)
        {
            let newEntry : (string|number)[] = [this.depthInterval[i], this.colorInterval[i]];
            returnArray.push(newEntry);
        }
        return returnArray;
    }

    public static getDepthIntervalPrettyPrint() : string[]
    {
        let depthIntervalTextual : string[] = this.depthInterval.map(String);
        let retArray : string[] = new Array(depthIntervalTextual.length);

        for (let i = 0; i < depthIntervalTextual.length - 1; i++)
        {
            retArray[i] = depthIntervalTextual[i] + "-" + depthIntervalTextual[i+1];
        }
        retArray[depthIntervalTextual.length - 1] = depthIntervalTextual[depthIntervalTextual.length - 1] + "+";
        return retArray;
    }
}