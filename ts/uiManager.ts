import { ChartManager } from "./chartManager"
import { ElementManager } from "./elementManager"
import { MapManager } from "./mapManager"
import { RectangleSelector } from "./rectangleSelector"

export class UiManager
{
    private _elementManager : ElementManager;
    private _chartManager : ChartManager;
    private _mapManager : MapManager;
    private _rectangleSelector : RectangleSelector;

    constructor()
    {
        this._elementManager = new ElementManager();
        this._mapManager = new MapManager();
        this._chartManager = new ChartManager(this._elementManager.getRadarCanvasContext(),
            this._elementManager.getBarCanvasContext(), this._elementManager.getCompCanvasContext());
        this._rectangleSelector = new RectangleSelector(this._mapManager, this._chartManager, this._elementManager);
    }

    public getElementManager() : ElementManager
    {
        return this._elementManager;
    }

    public getMapManager() : MapManager
    {
        return this._mapManager;
    }
}