import { Optional } from "./optional";

export async function requestFile(url : string)
{
    return new Promise<string>((resolve, reject) => {
        let xmlHttp : XMLHttpRequest = new XMLHttpRequest();

        xmlHttp.onreadystatechange = () =>
        {
            if (xmlHttp.readyState === XMLHttpRequest.DONE && xmlHttp.status === 200)
            {
                resolve(xmlHttp.responseText);
            }
        };
        xmlHttp.open("GET", url, true);
        xmlHttp.send();
    });
}
