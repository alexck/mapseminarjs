import { ClusterUtil } from "../ts/clusterUtil";
import * as GeoJson from "@types/geojson";
import { expect } from "chai"
import { TestHelper } from "./testHelper";

let testPropertiesOne : any = { clusterId : 1 };
let testPropertiesTwo : any = { clusterId : 5 };
let testPropertiesThree : any = { clusterId : 1337 };

describe("ClusterUtil", () => {
    it("should remove duplicates", () => {
        let returnedData = ClusterUtil.removeDuplicateClusters(getTestData())
        expect(returnedData.length).to.be.equal(3);
        expect(returnedData).to.be.deep.equal(getExpectedData());
    });
});

function getExpectedData() : GeoJson.Feature<GeoJson.Point>[]
{
    let expectedReturn : GeoJson.Feature<GeoJson.Point>[] = new Array();

    expectedReturn.push(TestHelper.getFeatureForProperty(testPropertiesOne));
    expectedReturn.push(TestHelper.getFeatureForProperty(testPropertiesTwo));
    expectedReturn.push(TestHelper.getFeatureForProperty(testPropertiesThree));
    
    return expectedReturn;
}

function getTestData() : GeoJSON.Feature<GeoJSON.Point>[]
{
    let featureReturn : GeoJson.Feature<GeoJson.Point>[] = new Array();

    featureReturn.push(TestHelper.getFeatureForProperty(testPropertiesOne));
    featureReturn.push(TestHelper.getFeatureForProperty(testPropertiesTwo));
    featureReturn.push(TestHelper.getFeatureForProperty(testPropertiesTwo));
    featureReturn.push(TestHelper.getFeatureForProperty(testPropertiesTwo));
    featureReturn.push(TestHelper.getFeatureForProperty(testPropertiesThree));
    featureReturn.push(TestHelper.getFeatureForProperty(testPropertiesThree)); 

    return featureReturn;
}