import { JsonParser, DateJson } from "./parser";
import { Optional } from "./optional";
import { Timer } from "./timer";
import { MapManager, MapModes } from "./mapManager";
import { DateManager } from "./dateManager";
import { ElementManager } from "./elementManager";
import { UiManager } from "./uiManager";
import { Config } from "./config";
import { WorkerMessenger } from "./workerSynchronizer";
import * as FileRequest from "./filerequest";
import * as GeoJson from "@types/geojson";
import * as ClusterWorker from "worker-loader?name=../../workerOut.js!./clusterWorker";

main();

async function main() {
    let uiManager : UiManager = new UiManager();
    let elementManager = uiManager.getElementManager();
    let mapManager = uiManager.getMapManager();
    let validDates : string = await FileRequest.requestFile("http://127.0.0.1:8080/out/validDates.json");
    let parsedDateJson : Optional<DateJson> = JsonParser.parseDateJson(validDates);
    let dateManager : DateManager = new DateManager(parsedDateJson.dataItem);
    const worker = new ClusterWorker();
    let workerMessenger : WorkerMessenger = new WorkerMessenger(worker);

    let playbackActive = false;
    let timer : Timer = new Timer(Config.getDefaultPlaybackSpeed(), async () => {
        if (workerMessenger.isWorkerReady())
        {
            let currentDate : string = dateManager.getCurrentDate();
            elementManager.setTimeRangeValue(dateManager.getCurrentDateIndex());
            loadAndRequestDate(currentDate, workerMessenger, mapManager);
            dateManager.increaseDate();
        }
    }); 
    elementManager.setTimeRangeValue(0);
    elementManager.setTimeRangeMaxValue(dateManager.getDatesCount() - 1);

    elementManager.onClusterButtonClick(() => {
        workerMessenger.requestUpdateClusterRadius(Config.getClusterMapRadius());
        loadAndRequestDate(dateManager.getCurrentDate(), workerMessenger, mapManager);
        mapManager.switchStyle(MapModes.Cluster);
    });

    elementManager.onHeatmapButtonClick(() => { 
        workerMessenger.requestUpdateClusterRadius(Config.getHeatMapRadius());
        loadAndRequestDate(dateManager.getCurrentDate(), workerMessenger, mapManager);
        mapManager.switchStyle(MapModes.Heatmap);
    });

    elementManager.onPlayButtonClick(() => 
    {
        if (playbackActive)
        {
            elementManager.hidePauseIcon();
            elementManager.showPlayIcon();
            playbackActive = false;
            timer.stop();
        }
        else
        {
            elementManager.showPauseIcon();
            elementManager.hidePlayIcon();
            playbackActive = true;
            timer.start();
        }
    });

    elementManager.onSpeedOneLabelClick(() =>
    {
        timer.changeStep(Config.getDefaultPlaybackSpeed());
    });

    elementManager.onSpeedTwoLabelClick(() => 
    {
        timer.changeStep(Config.getDefaultPlaybackSpeed() / 2);
    });

    elementManager.onSpeedThreeLabelClick(() => 
    {
        timer.changeStep(Config.getDefaultPlaybackSpeed() / 3);
    });

    elementManager.onZoomInButtonClick(() => 
    {
        mapManager.zoomIn();
    });

    elementManager.onZoomOutButtonClick(() => 
    {
        mapManager.zoomOut();
    });

    /*
    elementManager.onTimeSelectChange(() => {
        timer.changeStep(elementManager.getTimeSelectValue());
    });
    */

    elementManager.onTimeRangeChange(() => 
    {
        if (!playbackActive)
        {
            dateManager.setDateByIndex(elementManager.getTimeRangeValue());
            loadAndRequestDate(dateManager.getCurrentDate(), workerMessenger, mapManager);
        }
    }); 

    mapManager.onMove(() => 
    {
        if (!playbackActive)
        {
            workerMessenger.requestDataItems(mapManager.getBounds(), mapManager.getZoom());
        } 
    });

    workerMessenger.onWorkerResponse((geoResponse : GeoJson.FeatureCollection<GeoJson.Point>) =>
    {
        mapManager.updateSource(geoResponse);
        elementManager.setDateText(dateManager.getCurrentDatePrettyPrint());
    });
}

async function loadAndRequestDate(date : string, workerMessenger : WorkerMessenger, mapManager : MapManager)
{
    let geoJson : string = await FileRequest.requestFile("http://127.0.0.1:8080/out/" + date + ".geojson");
    let parsedGeoJson : Optional<GeoJson.FeatureCollection<GeoJson.Point>> = JsonParser.parseGeoJson(geoJson);

    if (parsedGeoJson.isPresent)
    {
        workerMessenger.requestLoadData(parsedGeoJson.dataItem.features);
        workerMessenger.requestDataItems(mapManager.getBounds(), mapManager.getZoom());
    }
}

