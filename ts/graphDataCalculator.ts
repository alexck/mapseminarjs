import { Optional } from "./optional";
import { Config } from "./config";

export class GraphDataCalculator
{
    public static calculateClusterDistribution(data : GeoJSON.Feature<GeoJSON.GeometryObject>[]) : number[]
    {
        let depthInterval = Config.getDepthInterval();
        let resultData : number[] = new Array(depthInterval.length).fill(0);

        for (let feature of data)
        {
            let snowDepth = 0;
            let featureGroupFound : boolean = false;

            try
            {
                snowDepth = this.snowDepthFromFeature(feature);
            }
            catch(e)
            {
                throw e;
            }
            
            for (let i = 0; i < depthInterval.length - 1; i++)
            {
                if (snowDepth >= depthInterval[i] && snowDepth < depthInterval[i+1])
                {
                    resultData[i]++;
                    featureGroupFound = true;
                    break;
                }
            }

            if (!featureGroupFound)
            {
                resultData[resultData.length - 1]++;
            }
        }
        return resultData;
    }

    public static calculateMinAvgMax(data : GeoJSON.Feature<GeoJSON.GeometryObject>[]) : number[]
    {
        let resultData : number[] = [0, 0, 0];
        let firstFeatureSnowDepth = 0;

        let currentMin = Number.MAX_VALUE;
        let currentMax = Number.MIN_VALUE;
        let currentSumAvg = 0;

        for (let feature of data)
        {
            let snowDepth = 0;

            try
            {
                snowDepth = this.snowDepthFromFeature(feature);
            }
            catch(e)
            {
                throw e;
            }

            if (snowDepth < currentMin)
            {
                currentMin = snowDepth;
            }
            if (snowDepth > currentMax)
            {
                currentMax = snowDepth;
            }
            currentSumAvg = currentSumAvg + snowDepth;
        }
        resultData[0] = currentMin;
        resultData[1] = currentSumAvg / data.length;
        resultData[2] = currentMax;
        return resultData;
    }

    private static snowDepthFromFeature(feature : GeoJSON.Feature<GeoJSON.GeometryObject>) : number
    {
        if (feature)
        {
            let featureProps : any = feature.properties;
            let parsedReducedProperties : any = JSON.parse(featureProps.reducedProperties);
            return parsedReducedProperties.snowDepth;
        }
        else
        {
            throw new TypeError("Parsing error, maybe data not initialized");
        }   
    }
}