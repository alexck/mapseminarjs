import { GraphDataCalculator } from "../ts/graphDataCalculator"
import { Config } from "../ts/config";
import * as GeoJson from "@types/geojson";
import { expect } from "chai";
import * as sinon from "sinon"
import { TestHelper } from "./testHelper";

describe("GraphDataCalculator", () => {
    it("should calculate a correct distribution", () => {
        let stubData : number[] = [0, 2, 40, 100];
        sinon.stub(Config, "getDepthInterval").returns(stubData);

        let distribution : number[] = GraphDataCalculator.calculateClusterDistribution(getTestData());
        expect(distribution).to.deep.equal([2, 1, 2, 2]);

        sinon.restore(Config.getDepthInterval);
    });

    it("should calculate min/max/avg", () => {
        let minMaxAvg : number[] = GraphDataCalculator.calculateMinAvgMax(getTestData());
        expect(minMaxAvg[0]).to.be.equal(0);
        expect(minMaxAvg[1]).to.be.closeTo(48.4286, 0.001);
        expect(minMaxAvg[2]).to.be.equal(101);
    });
});

function getTestData() : GeoJSON.Feature<GeoJSON.Point>[]
{
    let featureReturn : GeoJson.Feature<GeoJson.Point>[] = new Array();

    featureReturn.push(getFeature({ snowDepth : 0 }));
    featureReturn.push(getFeature({ snowDepth : 1 }));
    featureReturn.push(getFeature({ snowDepth : 30 }));
    featureReturn.push(getFeature({ snowDepth : 40 }));
    featureReturn.push(getFeature({ snowDepth : 67 }));
    featureReturn.push(getFeature({ snowDepth : 100 }));
    featureReturn.push(getFeature({ snowDepth : 101 }));
    
    return featureReturn;
}

function getFeature(testProperty : any) : GeoJSON.Feature<GeoJSON.Point>
{
    let stringified : string = JSON.stringify(testProperty);
    let reducedProps : any = { reducedProperties : stringified };
    return TestHelper.getFeatureForProperty(reducedProps);
}