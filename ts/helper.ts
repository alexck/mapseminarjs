import * as GeoJson from "@types/geojson";

/*
 * Die Methoden longitudeToMercator, latitudeToMercator, mercatorToLongitude, mercatorToLatitude
 * enstammen dem Code der Supercluster Library
 * Authoren: Vladimir Agafonkin, Andrew Harvey, Github Nutzer: jingsam
 * Source Code online unter: https://github.com/mapbox/supercluster/blob/master/index.js
 */

export function longitudeToMercator(longitude : number) : number
{
    return (longitude / 360) + 0.5;
}

export function latitudeToMercator(latitude : number) : number
{
    let sin : number = Math.sin(latitude * Math.PI / 180);
    let y : number = (0.5 - 0.25 * Math.log((1 + sin) / (1 - sin)) / Math.PI);
    return y < 0 ? 0 : y > 1 ? 1 : y;
}

export function mercatorToLongitude(mercatorX : number) : number
{
    return (mercatorX - 0.5) * 360;
}

export function mercatorToLatitude(mercatorY : number) : number
{
    let y = (180 - mercatorY * 360) * Math.PI / 180;
    return 360 * Math.atan(Math.exp(y)) / Math.PI - 90;
}

export function mapboxBoundsToMercator(boundingBox : BoundingBox)
{
    let minX = longitudeToMercator(boundingBox.southWestLng);
    let maxX = longitudeToMercator(boundingBox.northEastLng);
    let minY = latitudeToMercator(boundingBox.northEastLat);
    let maxY = latitudeToMercator(boundingBox.southWestLat);
    let mercatorBoundingBox : IMercatorBoundingBox = {
        minXPos : minX,
        maxXPos : maxX,
        minYPos : minY,
        maxYPos : maxY
    };
    return mercatorBoundingBox;
}

export interface IMercatorBoundingBox
{
    minXPos : number;
    maxXPos : number;
    minYPos : number;
    maxYPos : number;
}

export interface BoundingBox
{
    southWestLng : number,
    southWestLat : number,
    northEastLng : number,
    northEastLat : number,
}
