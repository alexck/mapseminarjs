import * as GeoJson from "@types/geojson";
import { Optional } from "./optional";
import { Cluster } from "./cluster";

export class JsonParser
{
    public static parseGeoJson(geoJson : string) : Optional<GeoJson.FeatureCollection<GeoJson.Point>>
    {
        let parsedJsonObject : any = JSON.parse(geoJson);

        if (this.isValidGeoJson(parsedJsonObject))
        {
            return Optional.of(parsedJsonObject);
        }
        else
        {
            return Optional.empty<GeoJson.FeatureCollection<GeoJson.Point>>();
        }
    }

    public static parseDateJson(dateJson : string) : Optional<DateJson>
    {
        let parsedJson : any = JSON.parse(dateJson);

        if (this.isValidDateJson(parsedJson))
        {
            return Optional.of(parsedJson);
        }
        else
        {
            return Optional.empty<DateJson>();
        }
    }

    public static getGeoJsonFromClusters(clusters : Cluster[]) : GeoJson.FeatureCollection<GeoJson.Point>
    {
        let geoJsonFeatures : GeoJson.Feature<GeoJson.Point>[] = new Array();

        for (let currentCluster of clusters)
        {
            geoJsonFeatures.push(currentCluster.getClusterGeoJson());
        }
        let collection : GeoJson.FeatureCollection<GeoJson.Point> = {
            type: "FeatureCollection",
            features : geoJsonFeatures
        }
        return collection;
    }

    private static isValidGeoJson(geoJson : any) : geoJson is GeoJson.FeatureCollection<GeoJson.Point>
    {
        return (geoJson as GeoJson.FeatureCollection<GeoJson.Point>).type !== undefined;
    }

    private static isValidDateJson(dateJson : any) : dateJson is DateJson
    {
        return (dateJson as DateJson).dates !== undefined;
    }
}

export interface DateJson
{
    dates : string[];
}
