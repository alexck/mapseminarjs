import { Chart } from "chart.js"
import { GraphDataCalculator } from "./graphDataCalculator";
import { Config } from "./config";
import * as GeoJson from "@types/geojson";

export class ChartManager
{
    private _radarCtx : CanvasRenderingContext2D;
    private _barCtx : CanvasRenderingContext2D;
    private _compCtx : CanvasRenderingContext2D;
    private _radarChart : Chart;
    private _barChart : Chart;
    private _compChart : Chart;

    constructor(radarCtx : CanvasRenderingContext2D, barCtx : CanvasRenderingContext2D, compCtx : CanvasRenderingContext2D)
    {
        this._radarCtx = radarCtx;
        this._barCtx = barCtx;
        this._compCtx = compCtx;
         (<any>Chart).plugins.register({
            beforeDraw: function(chartInstance : any) {
                let ctx = chartInstance.chart.ctx;
                ctx.fillStyle = "rgba(70, 70, 70, 1)";
                ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
        });
        (<any>Chart).defaults.global.defaultFontFamily = "Slabo 27px";

        this.initializeRadarChart();
        this.initializeBarChart();
        this.initializeCompChart();
    }

    public updateChartData(newData : GeoJSON.Feature<GeoJSON.GeometryObject>[])
    {
        let radarChartData : any = this._radarChart.data;
        let barChartData : any = this._barChart.data;
        let compChartData : any = this._compChart.data;

        try
        {
            radarChartData.datasets[0].data = GraphDataCalculator.calculateClusterDistribution(newData);
            let minAvgMax : number[] = GraphDataCalculator.calculateMinAvgMax(newData);
            barChartData.datasets[0].data = minAvgMax;
            compChartData.datasets[0].data = [178, minAvgMax[1]];
        }   
        catch(e)
        {
            console.log("Tried to select data with no data loaded");
        }
        
        this._radarChart.update();
        this._barChart.update();
        this._compChart.update();
    }

    private initializeRadarChart() : void
    {
        let radarConfiguration : any = {
            type: "radar",
            data: {
                labels: Config.getDepthIntervalPrettyPrint(),
                datasets: [
                    {
                        label: "Snow Depth Distribution",
                        backgroundColor: "rgba(255, 0, 0, 0.2)",
                        borderColor: "rgba(255, 0, 0, 0.3)",
                        pointBackgroundColor: "#ff0000"
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: "top",
                    labels: {
                        fontColor: "white"
                    }
                },
                scale: {
                    ticks: {
                        beginAtZero: true,
                        fontColor: "white",
                        showLabelBackdrop: false
                    },
                    pointLabels: {
                        fontColor: "white"
                    },
                    gridLines: {
                        color: 'rgba(255, 255, 255, 0.2)'
                    },
                    angleLines: {
                        color: "rgba(255, 255, 255, 0.3)"
                    }
                }
            }
        };
        this._radarChart = new Chart(this._radarCtx, radarConfiguration);
    }

    private initializeBarChart()
    {
        let barConfiguration : any = {
            type: "bar",
            data: {
                labels: ["Min", "Avg", "Max" ],
                datasets: [
                    {
                        label: "Snow Depth (cm)",
                        backgroundColor: "rgba(0, 0, 255, 0.2)",
                        borderColor: "rgba(0, 0, 255, 0.3)"
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: "top",
                    labels: {
                        fontColor: "white"
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            showLabelBackdrop: false
                        },
                        pointLabels: {
                            fontColor: "white"
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.2)'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            showLabelBackdrop: false
                        },
                        pointLabels: {
                            fontColor: "white"
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.2)'
                        }
                    }]
                }
            }
        };
        this._barChart = new Chart(this._barCtx, barConfiguration);
    }

    private initializeCompChart() : void
    {
        let compConfiguration : any = {
            type: "bar",
            data: {
                labels: ["Human", "Avg Snowdepth"],
                datasets: [
                    {
                        label: "Height (cm)",
                        backgroundColor: "rgba(0, 0, 255, 0.2)",
                        borderColor: "rgba(0, 0, 255, 0.3)"
                    }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: "top",
                    labels: {
                        fontColor: "white"
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            showLabelBackdrop: false
                        },
                        pointLabels: {
                            fontColor: "white"
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.2)'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            showLabelBackdrop: false
                        },
                        pointLabels: {
                            fontColor: "white"
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.2)'
                        }
                    }]
                }
            }
        };
        this._compChart = new Chart(this._compCtx, compConfiguration);
    }
}