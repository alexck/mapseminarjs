export class Optional<T>
{
    private dataItem_ : T;
    private isPresent_ : boolean;

    constructor(isPresent : boolean, dataItem : T)
    {
        this.dataItem_ = dataItem;
        this.isPresent_ = isPresent;
    }

    get isPresent()
    {
        return this.isPresent_;
    }

    get dataItem()
    {
        return this.dataItem_;
    }

    public static empty<T>() : Optional<T>
    {
        return new Optional(false, undefined);
    }

    public static of<T>(dataItem : T) : Optional<T>
    {
        return new Optional(true, dataItem);
    }
}
