import { Vector2 } from "../ts/vectorTwo";
import { expect } from "chai";

describe("Vector2", () => {
    let testVector : Vector2;

    beforeEach(() => {
        testVector = new Vector2(2.5, 80);
    });

    it("should return the correct x Position", () => {
        expect(testVector.xPos).to.be.equal(2.5);
    });

    it("should return the correct y Position", () => {
        expect(testVector.yPos).to.be.equal(80);
    });

    it("should create a correct [x,y] array", () => {
        expect(testVector.convertToArray()).to.deep.equal([2.5, 80]);
    })
});