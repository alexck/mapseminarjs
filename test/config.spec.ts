import { Config } from "../ts/config";
import { expect } from "chai";

describe("Config", () => {
    it("should return the correct depth interval", () => {
        expect(Config.getDepthInterval()).to.deep.equal([0, 2, 40, 100]);
    });

    it("should return the correct color array", () => {
        expect(Config.getColorInterval()).to.deep.equal(["#009933", "#00ccff", "#0066ff", "#6600cc"]);
    });

    it("should create a correct pritty print", () => {
        expect(Config.getDepthIntervalPrettyPrint()).to.deep.equal(["0-2", "2-40", "40-100", "100+"]);
    });

    it("should create a correct mapbox color interval", () => {
        let expectedInterval : (string|number)[][] = [
            [0, "#009933"],
            [2, "#00ccff"],
            [40, "#0066ff"],
            [100, "#6600cc"]
        ];
        expect(Config.getMapboxColorInterval()).to.deep.equal(expectedInterval);
    });
});