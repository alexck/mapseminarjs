import { BoundingBox } from "./helper";

export interface WorkerMessage
{
    type : MessageType,
    parameters : any
}

export interface DataRequestParameters
{
    boundingBox : BoundingBox,
    zoom : number
}

export enum MessageType
{
    LoadData,
    RequestData,
    UpdateClusterRadius,
    WorkerGeoDataResponse
}