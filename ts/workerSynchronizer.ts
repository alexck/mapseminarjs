import * as GeoJson from "@types/geojson";
import { WorkerMessage, MessageType, DataRequestParameters } from "./workerMessage";
import { BoundingBox } from "./helper";
import { Queue } from "./queue";

export class WorkerMessenger
{
    private _worker : Worker;
    private _queue : Queue<WorkerMessage>;

    constructor(worker : Worker)
    {
        this._worker = worker;
        this._queue = new Queue<WorkerMessage>();
    }

    public isWorkerReady() : boolean
    {
        return this._queue.isEmpty();
    }

    public requestLoadData(features: GeoJson.Feature<GeoJson.Point>[]): void {
        let requestDataLoad: WorkerMessage = {
            type: MessageType.LoadData,
            parameters: features
        };
        
        if (this._queue.isEmpty())
        {
            this._worker.postMessage(requestDataLoad);
        }
        else
        {
            this._queue.push(requestDataLoad);
        }
    }

    public requestUpdateClusterRadius(newRadius: number): void {
        let requestRadius: WorkerMessage = {
            type: MessageType.UpdateClusterRadius,
            parameters: newRadius
        };
        this._worker.postMessage(requestRadius);
    }

    public requestDataItems(bBox: mapboxgl.LngLatBounds, currentZoom: number) {
        let box: BoundingBox = {
            southWestLng: bBox.getSouthWest().lng,
            southWestLat: bBox.getSouthWest().lat,
            northEastLng: bBox.getNorthEast().lng,
            northEastLat: bBox.getNorthEast().lat,
        };
        let requestParameters: DataRequestParameters = {
            boundingBox: box,
            zoom: currentZoom
        };
        let requestDataItems: WorkerMessage = {
            type: MessageType.RequestData,
            parameters: requestParameters
        };
        this._worker.postMessage(requestDataItems);
    }

    public onWorkerResponse(receivedCallback : (response : GeoJson.FeatureCollection<GeoJson.Point>) => void)
    {
        this._worker.addEventListener('message', (message : any) => {
            let parsedMessage = JSON.parse(message.data);
            let convertedMessage : WorkerMessage = parsedMessage as WorkerMessage;
            let geoResponse = convertedMessage.parameters as GeoJson.FeatureCollection<GeoJson.Point>;

            if (!this._queue.isEmpty)
            {
                this._worker.postMessage(this._queue.pop());
            }
            receivedCallback(geoResponse);   
        }); 
    }
}