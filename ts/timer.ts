
export class Timer
{
    private _intervalId : number;
    private _step : Number;
    private _callback : () => void;
    private _running : boolean;

    constructor(step : Number, callback : () => void)
    {
        this._step = step;
        this._callback = callback;
        this._running = false;
    }

    public start()
    {
        this._running = true;
        this._intervalId = setInterval(this._callback, this._step);
    }

    public changeStep(newStep : number)
    {
        this._step = newStep;

        if (this._running)
        {
            this.stop();
            this.start();
        }
    }

    public stop()
    {
        this._running = false;
        clearInterval(this._intervalId);
    }
}