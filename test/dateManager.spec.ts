import { DateManager } from "../ts/dateManager";
import { DateJson } from "../ts/parser";
import { expect } from "chai"

describe("DateManager", () => {
    let dateManager : DateManager;
    let testData : string[] = ["1-1", "3-2", "10-5", "6-12"];
    let dateJson : DateJson = {
        dates : testData
    };

    beforeEach(() => {
        dateManager = new DateManager(dateJson);
    });

    it ("should increase dates correctly", () => {
        expect(dateManager.getCurrentDate()).to.be.equal("1-1");
        dateManager.increaseDate();
        expect(dateManager.getCurrentDate()).to.be.equal("3-2");
        dateManager.increaseDate();
        expect(dateManager.getCurrentDate()).to.be.equal("10-5");
        dateManager.increaseDate();
        expect(dateManager.getCurrentDate()).to.be.equal("6-12");
        dateManager.increaseDate();
        expect(dateManager.getCurrentDate()).to.be.equal("1-1");
        dateManager.increaseDate();
        expect(dateManager.getCurrentDate()).to.be.equal("3-2");
    });

    it ("should have the right amount of dates", () => {
        expect(dateManager.getDatesCount()).to.be.equal(4);
    });

    it ("should pretty print correctly", () => {
        expect(dateManager.getCurrentDatePrettyPrint()).to.be.equal("Fri Jan 01 2016 00:00:00 GMT+0100 (CET)");
        dateManager.increaseDate();
        expect(dateManager.getCurrentDatePrettyPrint()).to.be.equal("Wed Feb 03 2016 00:00:00 GMT+0100 (CET)");
    });

    it ("should set the date by index correctly", () => {
        dateManager.setDateByIndex(2);
        expect(dateManager.getCurrentDate()).to.be.equal("10-5");
        expect(() => dateManager.setDateByIndex(4)).to.throw(RangeError);
    });
});