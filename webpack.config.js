var path = require('path');

module.exports = {
 entry: './ts/index.ts',
 output: {
   filename: 'bundle.js',
   path: __dirname + '/dist' + '/bundle/'
 },
 module: {
        rules: [
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          exclude: /node_modules/,
        },
        {
          enforce: 'pre',
          test: /\.js$/,
          loader: "source-map-loader"
        },
        {
          enforce: 'pre',
          test: /\.tsx?$/,
          use: "source-map-loader"
        }
        ]
 },
 resolve: {
   extensions: [".tsx", ".ts", ".js"],
   alias: {
            jquery: path.join(__dirname, 'node_modules/jquery/dist/jquery')
    },
 },
 devtool: "source-map"
};